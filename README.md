# aoc-clojure

My solutions to advent of code challenges done in Clojure.
I use these as a fun way to get into the language a bit more.
