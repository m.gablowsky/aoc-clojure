(ns aoc-clojure.2022.day4-test
  (:require [aoc-clojure.2022.day4 :as sut]
            [clojure.test :as t]))

(def test-input '("2-4,6-8"
                  "2-3,4-5"
                  "5-7,7-9"
                  "2-8,3-7"
                  "6-6,4-6"
                  "2-6,4-8"))

(t/deftest part1-test
  (t/testing "Finds number of fully contained ranges within pairs"
    (t/are [expected actual] (= expected actual)
      2 (sut/part1 test-input))))

(t/deftest part2-test
  (t/testing "Finds number of partially overlapping ranges within pairs"
    (t/are [expected actual] (= expected actual)
      4 (sut/part2 test-input))))
