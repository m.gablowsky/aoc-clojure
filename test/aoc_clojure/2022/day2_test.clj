(ns aoc-clojure.2022.day2-test
  (:require [aoc-clojure.2022.day2 :as sut]
            [clojure.test :as t]))

(def test-input '("A Y" "B X" "C Z"))

(t/deftest part1-test
  (t/testing "Total score is calculated correctly"
    (t/are [expected actual] (= expected actual)
      15 (sut/part1 test-input))))

(t/deftest part2-test
  (t/testing "Total score is calculated correctly"
    (t/are [expected actual] (= expected actual)
      12 (sut/part2 test-input))))
