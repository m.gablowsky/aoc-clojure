(ns aoc-clojure.2022.day3-test
  (:require [aoc-clojure.2022.day3 :as sut]
            [clojure.test :as t]))

(def test-input '("vJrwpWtwJgWrhcsFMMfFFhFp"
                  "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
                  "PmmdzqPrVvPwwTWBwg"
                  "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
                  "ttgJtRGJQctTZtZT"
                  "CrZsJsPPZsGzwwsLwLmpwMDw"))

(t/deftest part1-test
  (t/testing "Sum of item type priority is calculated correctly"
    (t/are [expected actual] (= expected actual)
      157 (sut/part1 test-input))))

(t/deftest part2-test
  (t/testing "Sum of badge type priority is calculated correctly"
    (t/are [expected actual] (= expected actual)
      70 (sut/part2 test-input))))
