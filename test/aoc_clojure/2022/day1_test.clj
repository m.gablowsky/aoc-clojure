(ns aoc-clojure.2022.day1-test
  (:require [aoc-clojure.2022.day1 :as sut]
            [clojure.test :as t]))

(def test-input "1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000")

(t/deftest part1-test
  (t/testing "Calories are calculated correctly"
    (t/are [expected actual] (= expected actual)
      24000 (sut/part1 test-input))))

(t/deftest part2-test
  (t/testing "Calories are calculated correctly"
    (t/are [expected actual] (= expected actual)
      45000 (sut/part2 test-input))))
