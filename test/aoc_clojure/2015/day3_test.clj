(ns aoc-clojure.2015.day3-test
  (:require [aoc-clojure.2015.day3 :as sut]
            [clojure.test :refer [deftest testing are]]))

(deftest satisfies-part1-examples
  (testing "Calculates correct number of houses"
    (are [expected actual] (= expected actual)
      2 (sut/part1 ">")
      4 (sut/part1 "^>v<")
      2 (sut/part1 "^v^v^v^v^v"))))

(deftest satisfies-part2-examples
  (testing "Calculates correct number of houses"
    (are [expected actual] (= expected actual)
      3 (sut/part2 "^v")
      3 (sut/part1 "^>v<")
      11 (sut/part1 "^v^v^v^v^v"))))
