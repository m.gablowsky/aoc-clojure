(ns aoc-clojure.2015.day2-test
  (:require [aoc-clojure.2015.day2 :as sut]
            [clojure.test :refer [deftest testing are]]))

(deftest satisfies-part1-examples
  (testing "Calculates correct wrapping paper amounts"
    (are [expected actual] (= expected actual)
      58 (sut/part1 '("2x3x4"))
      43 (sut/part1 '("1x1x10")))))

(deftest satisfies-part2-examples
  (testing "Calculates correct ribbon amounts"
    (are [expected actual] (= expected actual)
      34 (sut/part2 '("2x3x4"))
      14  (sut/part2 '("1x1x10")))))
