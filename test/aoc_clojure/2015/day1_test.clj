(ns aoc-clojure.2015.day1-test
  (:require [aoc-clojure.2015.day1 :as sut]
            [clojure.test :refer [deftest testing are]]))

(deftest satisfies-part1-examples
  (testing "Finds correct floor number"
    (are [expected actual] (= expected actual)
      0 (sut/part1 "(())")
      0 (sut/part1 "()()")
      3 (sut/part1 "(()(()(")
      3 (sut/part1 "(((")
      3 (sut/part1 "))(((((")
      -1 (sut/part1 "())")
      -1 (sut/part1 "))(")
      -3 (sut/part1 ")))")
      -3 (sut/part1 ")())())"))))

(deftest satisfies-part2-examples
  (testing "Finds correct character position"
    (are [expected actual] (= expected actual)
      1 (sut/part2 ")")
      5 (sut/part2 "()())")
      3 (sut/part2 "()))(()()))()()")
      5 (sut/part2 "()())))()())()")
      9 (sut/part2 "(((()))))"))))
