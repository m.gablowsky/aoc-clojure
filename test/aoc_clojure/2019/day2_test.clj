(ns aoc-clojure.2019.day2-test
  (:require [aoc-clojure.2019.day2 :as sut]
            [clojure.test :refer [deftest testing are]]))

(defn- run-program [program [noun verb]]
  (->> (sut/run-with-params program [noun verb])
       :memory))

(deftest machine-works-as-specified
  (testing "Programs produce correct state"
    (are [expected actual] (= expected actual)
      [2 0 0 0 99] (run-program "1,0,0,0,99" [0 0])
      [2 3 0 6 99] (run-program "2,3,0,3,99" [3 0])
      [2 4 4 5 99 9801] (run-program "2,4,4,5,99,0" [4 4])
      [30 1 1 4 2 5 6 0 99] (run-program "1,1,1,4,99,5,6,0,99" [1 1]))))
