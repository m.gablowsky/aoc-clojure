(ns aoc-clojure.2019.day1-test
  (:require [aoc-clojure.2019.day1 :as sut]
            [clojure.test :refer [deftest testing are]]))

(deftest satisfies-part1-examples
  (testing "Fuel examples are calculated correctly"
    (are [expected actual] (= expected actual)
      2.0 (sut/part1 [12])
      2.0 (sut/part1 [14])
      654.0 (sut/part1 [1969])
      33583.0 (sut/part1 [100756]))))

(deftest satisfies-part2-examples
  (testing "Fuel examples are calculated correctly"
    (are [expected actual] (= expected actual)
      2.0 (sut/part2 [14])
      966.0 (sut/part2 [1969])
      50346.0 (sut/part2 [100756]))))
