(ns aoc-clojure.2024.day1-test
  (:require [aoc-clojure.2024.day1 :as sut]
            [clojure.test :refer [deftest testing are]]))

(def l1 [3 4 2 1 3 3])
(def l2 [4 3 5 3 9 3])

(deftest total-distance-between-lists
  (testing "Total distance is calculated correctly"
    (are [expected actual] (= expected actual)
      11 (sut/part1 l1 l2))))

(deftest similarity-score
  (testing "Similarity score is calculated correctly"
    (are [expected actual] (= expected actual)
      31 (sut/part2 l1 l2))))
