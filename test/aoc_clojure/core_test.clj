(ns aoc-clojure.core-test
  (:require [clojure.test :refer [deftest testing are is]]
            [aoc-clojure.core :as c]))

(deftest resource-file-resolution
  (testing "Path is correctly created"
    (are [generated expected] (= generated expected)
      (c/gen-input-file-name "test" 1) "resources/test/input-day1"
      (c/gen-input-file-name 2019 3) "resources/2019/input-day3")))

(deftest resource-loading
  (testing "Single line input is loaded correctly"
    (is (= (c/read-single-line-input "test" 1) "stuffonmultiplelines")))
  (testing "Multi line input is loaded correctly"
    (is (= (c/read-multi-line-input "test" 1) ["stuff" "on" "multiple" "lines"]))))
