(ns aoc-clojure.core
  (:gen-class)
  (:require [clojure.string :as s]))

(defn gen-input-file-name [year day]
  (str "resources/" year "/input-day" day))

(defn read-raw-input [year day]
  (slurp (gen-input-file-name year day)))

(defn read-single-line-input [year day]
  (s/replace (slurp (gen-input-file-name year day)) "\n" ""))

(defn read-multi-line-input [year day]
  (s/split (slurp (gen-input-file-name year day)) #"\n"))
