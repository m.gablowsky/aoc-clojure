(ns aoc-clojure.2015.day3)

;; https://adventofcode.com/2015/day/3

(defmulti move (fn [_ direction] direction))
(defmethod move \> [[x y] _] [(inc x) y])
(defmethod move \< [[x y] _] [(dec x) y])
(defmethod move \^ [[x y] _] [x (inc y)])
(defmethod move \v [[x y] _] [x (dec y)])

(defn- path [coords directions]
  (reductions move coords directions))

(defn part1 [input]
  (count (distinct (path [0 0] input))))

(defn part2 [input])
