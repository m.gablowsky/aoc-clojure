(ns aoc-clojure.2015.core
  (:require [aoc-clojure.2015.day1 :as day1]
            [aoc-clojure.2015.day2 :as day2]
            [aoc-clojure.2015.day3 :as day3]
            [aoc-clojure.core :as ac]))

(defn solve-day1 []
  (let [input (ac/read-single-line-input 2015 1)]
    [:part1 (day1/part1 input)
     :part2 (day1/part2 input)]))

(defn solve-day2 []
  (let [input (ac/read-multi-line-input 2015 2)]
    [:part1 (day2/part1 input)
     :part2 (day2/part2 input)]))

(defn solve-day3 []
  (let [input (ac/read-single-line-input 2015 3)]
    [:part1 (day3/part1 input)
     :part2 (day3/part2 input)]))
