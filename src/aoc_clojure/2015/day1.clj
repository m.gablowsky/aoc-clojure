(ns aoc-clojure.2015.day1)

;; https://adventofcode.com/2015/day/1

(def values {\( 1
             \) -1})

(defn convert-input [input]
  (map values (lazy-seq input)))

(defn part1 [input]
  (reduce + (convert-input input)))

(defn part2 [input]
  (->> (convert-input input)
       (reductions +)
       (take-while #(<= 0 %))
       count
       inc))
