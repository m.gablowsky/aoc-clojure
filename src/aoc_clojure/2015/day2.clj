(ns aoc-clojure.2015.day2
  (:require [clojure.string :as s]))

;; https://adventofcode.com/2015/day/2

(defn- to-dimensions [input]
  (mapv #(Integer/parseInt %) (s/split input #"x")))

(defn- calculate-areas [[l w h]]
  [(* l w) (* w h) (* h l)])

(defn- calculate-wrapping-paper [dimensions]
  (let [areas (calculate-areas dimensions)
        smallest-side (apply min areas)]
    (->> areas
         (map #(* 2 %))
         (reduce +)
         (+ smallest-side))))

(defn- calculate-length [[l w]]
  (+ l l w w))

(defn- calculate-ribbon [dimensions]
  (->> (sort dimensions)
       (calculate-length)
       (+ (reduce * dimensions))))

(defn- calculate [input calculator]
  (->> input
       (map to-dimensions)
       (map calculator)
       (reduce +)))

(defn part1 [input]
  (calculate input calculate-wrapping-paper))

(defn part2 [input]
  (calculate input calculate-ribbon))
