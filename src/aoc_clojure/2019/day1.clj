(ns aoc-clojure.2019.day1)

;; https://adventofcode.com/2019/day/1

(defn calc-fuel [mass]
  (- (Math/floor (/ mass 3)) 2))

(defn part1 [masses]
   (->> masses
        (map calc-fuel)
        (reduce +)))

(defn calc-fuel-recur [mass]
  (let [fuel (calc-fuel mass)]
    (if (<= fuel 0)
      0
      (+ fuel (calc-fuel-recur fuel)))))

(defn part2 [masses]
  (->> masses
       (map calc-fuel-recur)
       (reduce +)))
