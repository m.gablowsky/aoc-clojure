(ns aoc-clojure.2019.day2
  (:require [clojure.string :as s]))

;; https://adventofcode.com/2019/day/2

(defonce opcode-map {1 +
                     2 *})

(defn- create-memory-state [program]
  (into [] (map #(Integer/parseInt %) (s/split program #","))))

(defn- init-machine [program]
  {:memory (create-memory-state program)
   :inst-pointer 0
   :halted false})

(defn- fetch-opcode [{:keys [memory inst-pointer]}]
  (nth memory inst-pointer))

(defn- param-val [memory inst-pointer param-id]
  (nth memory (nth memory (+ inst-pointer param-id))))

(defn- do-op [memory inst-pointer op]
  (assoc memory (nth memory (+ inst-pointer 3)) (apply op [(param-val memory inst-pointer 1) (param-val memory inst-pointer 2)])))

(defn- execute-instruction [{:keys [memory inst-pointer] :as machine} opcode]
  (let [operation (opcode-map opcode)]
    (-> machine
        (assoc :memory (do-op memory inst-pointer operation))
        (assoc :inst-pointer (+ inst-pointer 4)))))

(defn- run-machine [{:keys [halted] :as machine}]
  (if (false? halted)
    (let [opcode (fetch-opcode machine)]
      (if (= opcode 99)
        (assoc machine :halted true)
        (recur (execute-instruction machine opcode))))))

(defn- set-memory [{ :keys [memory] :as machine} addr val]
    (assoc machine :memory (assoc memory addr val)))

(defn run-with-params [program [noun verb]]
  (-> (init-machine program)
      (set-memory 1 noun)
      (set-memory 2 verb)
      (run-machine)))

(defn part1 [program]
  (first (:memory (run-with-params program [12 2]))))

(defn- calculate-result [[noun verb]]
  (+ (* 100 noun) verb))

(defn- next-inputs [[noun verb]]
  (if (< verb 99)
    [noun (inc verb)]
    [(inc noun) 0]))

(defn part2 [program]
  (loop [inputs [0 0]]
    (if (= 19690720 (first (:memory (run-with-params program inputs))))
      (calculate-result inputs)
      (recur (next-inputs inputs)))))
