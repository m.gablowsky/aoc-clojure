(ns aoc-clojure.2019.core
  (:require [aoc-clojure.2019.day1 :as day1]
            [aoc-clojure.2019.day2 :as day2]
            [aoc-clojure.core :as ac]))

(defn solve-day1 []
  (let [input (map #(Integer/parseInt %) (ac/read-multi-line-input 2019 1))]
    [:part1 (day1/part1 input)
     :part2 (day1/part2 input)]))

(defn solve-day2 []
  (let [input (ac/read-single-line-input 2019 2)]
    [:part1 (day2/part1 input)
     :part2 (day2/part2 input)]))
