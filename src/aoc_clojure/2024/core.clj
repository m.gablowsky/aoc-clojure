(ns aoc-clojure.2024.core
  (:require [aoc-clojure.core :as ac]
            [aoc-clojure.2024.day1 :as day1]
            [clojure.string :as str]))

(defn solve-day1 []
  (let [input (->> (ac/read-multi-line-input 2024 1)
                   (map #(str/split % #"   ")))
        l1 (->> input
                (map first)
                (map #(Integer/parseInt %)))
        l2 (->> input
                (map second)
                (map #(Integer/parseInt %)))]
    [:part1 (day1/part1 l1 l2)
     :part2 (day1/part2 l1 l2)]))
