(ns aoc-clojure.2024.day1)

;; https://adventofcode.com/2024/day/1

(defn part1 [l1 l2]
  (reduce + (map #(abs (- %1 %2)) (sort l1) (sort l2))))

(defn part2 [l1 l2]
  (let [freqs (frequencies l2)]
    (reduce + (map #(* % (or (freqs %) 0)) l1))))
