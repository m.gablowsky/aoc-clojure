(ns aoc-clojure.2022.day4
  (:require [clojure.string :as s]))

; https://adventofcode.com/2022/day/4

(defn ->range [range-string]
  (let [range-limits (s/split range-string #"-")]
    [(Integer/parseInt (first range-limits))
     (Integer/parseInt (second range-limits))]))

(defn ->ranges [input-line]
  (map ->range (s/split input-line #",")))

(defn fully-overlaps? [[r1s r1e] [r2s r2e]]
  (or (<= r1s r2s r2e r1e)
      (<= r2s r1s r1e r2e)))

(defn part1 [input]
  (count (filter #(fully-overlaps? (first %) (second %)) (map ->ranges input))))

(defn partially-overlaps? [[r1s r1e] [r2s r2e]]
  (or (<= r1s r2s r1e)
      (<= r2s r1s r2e)))

(defn part2 [input]
  (count (filter #(partially-overlaps? (first %) (second %)) (map ->ranges input))))
