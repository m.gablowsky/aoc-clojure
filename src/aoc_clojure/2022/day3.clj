(ns aoc-clojure.2022.day3
  (:require [clojure.set :as cs]))

; https://adventofcode.com/2022/day/3

(defn find-shared-item [rucksack]
  (let [comp-size (/ (count rucksack) 2)
        comp1 (set (take comp-size rucksack))
        comp2 (set (drop comp-size rucksack))]
    (first (cs/intersection comp1 comp2))))

(def lowercase-offset 96) ;a is ascii 97 but shall be priority 1
(def uppercase-offset 38) ;A is ascii 65 but shall be priority 27

(defn ->priority [char]
  (if (Character/isUpperCase char)
    (- (int char) uppercase-offset)
    (- (int char) lowercase-offset)))

(defn part1 [input]
  (->> input
       (map find-shared-item)
       (map ->priority)
       (reduce +)))

(defn find-badge-item [rucksacks]
  (let [a (set (first rucksacks))
        b (set (second rucksacks))
        c (set (nth rucksacks 2))]
    (first (cs/intersection a b c))))

(defn part2 [input]
  (->> input
       (partition 3)
       (map find-badge-item)
       (map ->priority)
       (reduce +)))
