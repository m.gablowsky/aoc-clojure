(ns aoc-clojure.2022.day2
  (:require [clojure.string :as s]))

; https://adventofcode.com/2022/day/2

;; Rock defeats Scissors, Scissors defeats Paper, and Paper defeats Rock

;; A -> Rock     <- X -> Loose
;; B -> Paper    <- Y -> Draw
;; C -> Scissors <- Z -> Win

;; Total score is sum of scores for each round

;; Scoring:
;; Selected shape - 1 Rock, 2 Paper, 3 Scissors
;; Outcome - 0 Loss, 3 Draw, 6 Win

(def result->score {
                  "A A" 4
                  "A B" 8
                  "A C" 3
                  "B A" 1
                  "B B" 5
                  "B C" 9
                  "C A" 7
                  "C B" 2
                  "C C" 6})

(defn ->result [input]
  (-> input
       (s/replace #"X" "A")
       (s/replace #"Y" "B")
       (s/replace #"Z" "C")))

(defn part1 [input]
  (->> input
       (map ->result)
       (map result->score)
       (reduce +)))

(def input->result {
                    "A Y" "A A"
                    "A Z" "A B"
                    "A X" "A C"
                    "B X" "B A"
                    "B Y" "B B"
                    "B Z" "B C"
                    "C Z" "C A"
                    "C X" "C B"
                    "C Y" "C C"})

(defn part2 [input]
  (->> input
       (map input->result)
       (map result->score)
       (reduce +)))
