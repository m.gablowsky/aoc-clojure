(ns aoc-clojure.2022.day1
  (:require [clojure.string :as s]))

; https://adventofcode.com/2022/day/1

(defn- strs->ints [coll]
  (->> coll
       (filter #(not (clojure.string/blank? %)))
       (map #(Integer/parseInt %))))

(defn- transform-input [input]
  (->> (s/split input #"\n\n")  ;Empty Line means next elf
       (map #(s/split % #"\n")) ;Separate calories for each elf
       (map strs->ints)         ;Convert to number type
       (map #(reduce + %))      ;Compute total calories of each elf
       sort
       reverse))                ;Sort descending

(defn part1 [input]
  (->> (transform-input input)
       first))

(defn part2 [input]
  (->> (transform-input input)
       (take 3)
       (reduce +)))
