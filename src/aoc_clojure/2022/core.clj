(ns aoc-clojure.2022.core
  (:require
   [aoc-clojure.2022.day1 :as day1]
   [aoc-clojure.2022.day2 :as day2]
   [aoc-clojure.2022.day3 :as day3]
   [aoc-clojure.2022.day4 :as day4]
   [aoc-clojure.core :as ac]))

(defn solve-day1 []
  (let [input (ac/read-raw-input 2022 1)]
    [:part1 (day1/part1 input)
     :part2 (day1/part2 input)]))

(defn solve-day2 []
  (let [input (ac/read-multi-line-input 2022 2)]
    [:part1 (day2/part1 input)
     :part2 (day2/part2 input)]))

(defn solve-day3 []
  (let [input (ac/read-multi-line-input 2022 3)]
    [:part1 (day3/part1 input)
     :part2 (day3/part2 input)]))

(defn solve-day4 []
  (let [input (ac/read-multi-line-input 2022 4)]
    [:part1 (day4/part1 input)
     :part2 (day4/part2 input)]))
